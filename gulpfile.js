var app = './templates/';
var assets = './assets/';
var build = './_build/';

var gulp = require('gulp'),
    concat = require("gulp-concat"),
    rename = require("gulp-rename"),
    notify = require('gulp-notify'),
    connect = require('gulp-connect'),
    cache = require('gulp-cache'),
    preprocess = require('gulp-preprocess'),
    sass = require('gulp-sass'),
    size = require('gulp-filesize'),
    twig = require('gulp-twig'),
    newer = require('gulp-newer'),
    rigger = require('gulp-rigger'),
    imagemin = require('gulp-imagemin'),
    sourcemaps = require('gulp-sourcemaps'),
    pngquant = require('imagemin-pngquant');

var zip = require('gulp-zip');

var data = require('gulp-data');
var fs = require('fs');

var timestamp = Math.round(+new Date());

// server connect
gulp.task('connect', gulp.series(function (done) {
    connect.server({
        root: build,
        port: 9325,
        livereload: true
    });
    done();
}));

// sass
gulp.task('sass', gulp.series(function () {
    return gulp.src(assets + 'scss/styles.scss')
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(preprocess({context: {VER: timestamp}}))

        .pipe(sourcemaps.write())

        .pipe(gulp.dest(build + 'assets/css'))
        .pipe(size())
        .pipe(connect.reload())
        .pipe(notify('SASS compiled!'));
}));

gulp.task('twig', gulp.series((done) => {

    gulp.src([
        app + 'pages/*.twig'
    ])
        .pipe(data(function () {
            return JSON.parse(fs.readFileSync(assets + '/fixtures/data.json'));
        }))

        .pipe(twig({
            base: app,
            defaults: {cache: false},
            extend: function (Twig) {
                Twig.exports.extendFilter("price", function (a) {
                    return a;
                });
                Twig.exports.extendFilter("strftime", function (a) {
                    return a;
                });
                Twig.exports.extendFilter("phone", function (a) {
                    return a;
                });
            }
        }))
        .pipe(rename({extname: ""}))
        .pipe(gulp.dest(build));

    done();
}));

// images
gulp.task('img', function () {

    return gulp.src(assets + 'img/*')
        .pipe(newer(build + 'assets/img'))
        .pipe(cache(imagemin({
            progressive: true,
            use: [pngquant({optimizationLevel: 3})]
        })))
        .pipe(gulp.dest(build + 'assets/img'))
        .pipe(connect.reload());
});


gulp.task('svg', function () {

    return gulp.src(assets + 'svg/*')
        .pipe(newer(build + 'assets/svg'))
        .pipe(gulp.dest(build + 'assets/svg'))
        .pipe(connect.reload());
});

gulp.task('uploads', function () {
    return gulp.src(assets + 'uploads/*')
        .pipe(newer(build + '/uploads/*'))
        .pipe(cache(imagemin({
            progressive: true,
            use: [pngquant({optimizationLevel: 3})]
        })))
        .pipe(gulp.dest(build + '/uploads'))
        .pipe(connect.reload());
});

// watch
gulp.task('watch', gulp.series(function (done) {
    gulp.watch([
        assets + 'scss/*.scss',
        assets + 'scss/partials/**/*.scss'
    ], gulp.series(['sass']))
        .on('change', function (path, stats) {
            console.log(path);
        });

    gulp.watch([
        app + '**/*.twig',
        assets + '/fixtures/*.json',
    ], gulp.series(['twig']))
        .on('change', function (path, stats) {
            console.log(path);
        });

    gulp.watch([assets + 'js/*.js'], gulp.series(['js']));

    done();
}));

// zip build
gulp.task('zip', function () {
    return gulp.src(build + '**')
        .pipe(zip('build.zip'))
        .pipe(gulp.dest('.'));
});


gulp.task("js", gulp.series(function (done) {
    gulp
        .src(assets + "js/vendor.js")
        .pipe(rigger())
        .pipe(concat("script.js"))
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest(build + 'assets/js'));

    done();
}));

gulp.task('default', gulp.series(['twig', 'sass', 'js', 'img', 'svg', 'uploads', 'connect', 'watch']));