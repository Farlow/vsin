jQuery.each( [ "put", "delete", "patch" ], function( i, method ) {
    jQuery[ method ] = function( url, data, callback, type ) {
        if ( jQuery.isFunction( data ) ) {
            type = type || callback;
            callback = data;
            data = undefined;
        }

        return jQuery.ajax({
            url: url,
            type: method,
            dataType: type,
            data: data,
            success: callback
        });
    };
});

var page = {
    init: function () {
        for (var a in page) {
            try {
                if (typeof page[a]['init'] === 'function') page[a].init();
            } catch (e) {
                console.log(e);
            }

        }

    },

    document: {
        events: {
            click: function (event) {

                if (event.which === 3 || !event.originalEvent)
                    return;

                var objs = page.document.callbacks.click;
                for (var i in objs) {
                    if (typeof event.originalEvent.skip === 'undefined' || event.originalEvent.skip !== i) {
                        var obj = objs[i];
                        obj.arguments.unshift(event);
                        obj.callback.apply(obj.context, obj.arguments);
                        page.document.removeListener('click', i);
                    }
                }
            }
        },
        callbacks: {
            click: {}
        },
        init: function () {
        },
        addListener: function (event) {

            event = Object.assign({
                type: 'click',
                name: 'defaultName',
                context: null,
                arguments: [],
                callback: function () {
                },
            }, event);

            if ($.isEmptyObject(page.document.callbacks[event.type])) {

                $(document).on(event.type + '.page', page.document.events[event.type]);
            }

            if ((event.type in page.document.callbacks) === false)
                page.document.callbacks[event.type] = [];

            page.document.callbacks[event.type][event.name] = event;
        },
        removeListener: function (event, name) {

            if (typeof page.document.callbacks[event] === 'undefined')
                return false;

            if (typeof page.document.callbacks[event][name] !== 'undefined')
                delete page.document.callbacks[event][name];

            if ($.isEmptyObject(page.document.callbacks[event])) {
                $(document).off(event + '.page');
                page.document.callbacks[event] = {};
            }
        }
    },

    form: {

        obj: {},
        iframeId: null,

        init: function () {

            $('input[name="params[send]"]').val(1);

            $('button.submit, input.submit', 'form').on('click', function () {
                var form = $(this).closest('form');
                //console.log(form)
                $(form).trigger('submit');
            });

            $('form.validate').on('submit', function (event) {
                page.form.obj = this;
                return page.form.validate(event, this);
            });
            /**/
            $('form.validate').on('blur', 'input,textarea,select', function (event) {
                page.form.obj = this;
                return page.form.validate(event, this);
            });

            $('form.ajax').on('submit', function (event) {
                page.form.obj = this;
                $(this).addClass('_sending_');

                if ($('input[type="file"]', page.form.obj).length)
                    return page.form.ajaxIframe(event, this);

                return page.form.ajax(event, this);
            });

        },

        validate: function (event, obj) {

            var errors = false;
            var is = {
                _email: function () {
                    return /^.+@.+\..+$/i.test($(this).val());
                },
                _required: function () {
                    return $(this).val() !== '';
                },
                _phone: function () {
                    return /^((7|8)?[2-9]\d{2}[2-9]\d{2}\d{4}|\d{7})$/.test($(this).val().replace(/\D/g, ""))
                }
            };
            var check = function () {
                if ($(this)[0].hasAttribute("disabled")) {
                    $(this).removeClass('error');
                    return true;
                }
                var v = $(this).data('validate');
                if (!v) {
                    $(this).removeClass('error');
                    return true;
                }

                if ($(this)[0].type === 'radio') {
                    var val = $('input[type="radio"][name="' + $(this)[0].name + '"]:checked', obj).length;
                    console.log($('input[type="radio"][name="' + $(this)[0].name + '"]', obj), $(this)[0].name, 'obj', obj);
                    if (!val) {
                        errors = true;
                        $('input[type="radio"][name="' + $(this)[0].name + '"]', obj).addClass('error');
                        return false;
                    } else {
                        $('input[type="radio"][name="' + $(this)[0].name + '"]', obj).removeClass('error');
                        return true;
                    }
                }

                var vv = v.split(' ');
                for (var a in vv) {
                    var fn = '_' + vv[a];
                    if (typeof is[fn] === 'function') {
                        if (!is[fn].call(this)) {
                            errors = true;
                            $(this).addClass('error').removeClass('valid');
                        } else {
                            $(this).removeClass('error').addClass('valid');
                        }
                    } else {
                        $(this).removeClass('error');
                    }
                }
            };

            if (obj.tagName === 'FORM')
                $('input,textarea,select', obj).each(check);
            else check.call(obj);


            if (errors) {
                event.stopImmediatePropagation();
                return false;
            }

        },

        ajaxIframe: function (e, form) {


            // Create the iframe...
            var iframe = document.createElement("iframe");
            iframe.setAttribute("id", "upload_iframe");
            iframe.setAttribute("name", "upload_iframe");
            iframe.setAttribute("width", "0");
            iframe.setAttribute("height", "0");
            iframe.setAttribute("border", "0");
            iframe.setAttribute("style", "width: 0; height: 0; border: none; display:none;");

            // Add to document...
            form.parentNode.appendChild(iframe);
            window.frames['upload_iframe'].name = "upload_iframe";

            page.form.iframeId = document.getElementById("upload_iframe");

            var eventHandler = function () {
                var iframeId = page.form.iframeId;

                if (iframeId.detachEvent) iframeId.detachEvent("onload", eventHandler);
                else iframeId.removeEventListener("load", eventHandler, false);

                var content;
                if (iframeId.contentDocument) {
                    content = iframeId.contentDocument.body.innerHTML;
                } else if (iframeId.contentWindow) {
                    content = iframeId.contentWindow.document.body.innerText;
                } else if (iframeId.document) {
                    content = iframeId.document.body.innerHTML;
                }

                $(page.form.obj).removeClass('_sending_').addClass('_sent_');
                page.form.result(page.form.parseString(content));

                iframeId.parentNode.removeChild(iframeId);
            };

            if (page.form.iframeId.addEventListener) page.form.iframeId.addEventListener("load", eventHandler, true);
            if (page.form.iframeId.attachEvent) page.form.iframeId.attachEvent("onload", eventHandler);

            form.setAttribute("target", "upload_iframe");
            form.setAttribute("enctype", "multipart/form-data");
            form.setAttribute("encoding", "multipart/form-data");

            form.submit();
        },

        parseString: function (str) {
            try {
                return JSON.parse(str);
            } catch (e) {
                console.log(e);
                return false;
            }
        },

        ajax: function (e, obj) {
            $.post($(obj).attr('action'), $(obj).serialize(), function (data) {
                page.form.result.call(obj, data);
            }, "json");
            return false;
        },

        result: function (data) {
            $(this).removeClass('_sending_').addClass('_sent_');

            if (typeof data === 'object') {
                if (typeof data.text === 'string')
                    $('.result', this).text(data.text);

                if (typeof data.text === 'object')
                    for (var a in data.text)
                        $('.result', this).append('<li>' + data.text[a] + '</li>');

                if (typeof data.html === 'string')
                    $('.result', this).html(data.html);

                if (typeof data.errors === 'object') {
                    $('.errors', this).empty();
                    for (var a in data.errors) {
                        $('.errors', this).append('<li class="error">' + data.errors[a] + '</li>');
                        console.log(a, 'input[name*="[' + a + ']"]', this, $('input[name*="[' + a + ']"]', this));
                        $('input[name*="[' + a + ']"]', this).addClass('error');
                    }

                }
            }

            if (typeof data === 'object') {
                if ('remove' in data)
                    $(data.remove).remove();

                if ('location' in data)
                    if (data.location.url === 'reload')
                        window.setTimeout("window.location.href = window.location.href.split('#')[0]", data.location.delay);
                    else
                        window.setTimeout(function () {
                            console.log(data);
                            window.location.href = data.location.url;
                        }, data.location.delay);

                if ('append' in data)
                    for (a in data.append) {
                        $(a).append(data.append[a]);
                    }

                if ('notice' in data)
                    for (a in data.notice)
                        page.notice.show(a, data.notice[a]);

                if ('reset' in data)
                    $(page.form.obj).get(0).reset();

                if ('hide' in data) {
                    $(data.hide).hide();
                }
                if ('show' in data) {
                    $(data.show).show();
                }
                if ('fadeIn' in data) {
                    $(data.fadeIn).fadeIn();
                }
                if (data.dataLayer) {
                    window.dataLayer = window.dataLayer || [];
                    window.dataLayer.push(data.dataLayer);
                }
            }
        }

    },

    cookie: {
        get: function (name) {
            var matches = document.cookie.match(new RegExp(
                "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
            ));
            if (!matches)
                return undefined;

            var a = decodeURIComponent(matches[1]);
            try {
                a = JSON.parse(a);
            } catch (e) {
                return a;
            }
            return a;
        },

        set: function (name, value, options) {
            options = options || {};

            var expires = options.expires;

            if (typeof expires === "number" && expires) {
                var d = new Date();
                d.setTime(d.getTime() + expires * 1000);
                expires = options.expires = d;
            }
            if (expires && expires.toUTCString) {
                options.expires = expires.toUTCString();
            }
            options.path = '/';
            value = encodeURIComponent(value);

            var updatedCookie = name + "=" + value;

            for (var propName in options) {
                updatedCookie += "; " + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }

            document.cookie = updatedCookie;

            return true;
        },

        del: function (name) {
            page.cookie.set(name, "", {expires: -1});
            return true;
        }


    },


    image: {

        timeoutId: null,
        list: [],
        queue: [],
        loading: [],
        class: {
            loading: 'img_loading',
            ready: 'img_ready',
            error: 'img_loading_error'
        },
        max: 5,


        add2load: function (img, src) {
            if (!src)
                return;
            $(img).css('opacity', 0)
                .parent()
                .addClass(page.image.class.loading);

            img.onload = function () {

                $(img).css('opacity', 1)

                    .parent()
                    .removeClass(page.image.class.loading)
                    .addClass(page.image.class.ready);

                if (page.image.list.length) {
                    page.image.queue.push(page.image.list.splice(0, 1)[0]);
                    page.image.load();
                }
            };
            img.onerror = function () {

                $(img).parent()
                    .removeClass(page.image.class.loading)
                    .addClass(page.image.class.error);
                $(img).remove();

                if (page.image.list.length) {
                    page.image.queue.push(page.image.list.splice(0, 1)[0]);
                    page.image.load();
                }
            };

            if (page.image.queue.length < page.image.max)
                page.image.queue.push([img, src]);
            else
                page.image.list.push([img, src]);

            if (page.image.queue.length === 1)
                page.image.load();
        },

        load: function () {
            var e = new Error('');

            while (true) {
                if (!page.image.queue.length)
                    break;
                var el = page.image.queue.splice(0, 1)[0];
                el[0].src = el[1];
            }
        },

        load1: function (obj, src) {

            $(obj).attr("src", "/assets/img/loading/fff-000.gif");

            var img = new Image;
            img.onerror = function () {
                $(obj).hide();
            };
            img.onload = function () {
                $(obj).hide().attr('src', img.src).parent().removeClass(page.image.class_loading);
                $(obj).fadeIn();
                $(obj).trigger('load');
            };
            img.src = src;
        }
    },

    mask: {
        init: function () {

            $('input[name="values[phone]"]')
                .on('paste', function (e) {

                    var data = e.originalEvent.clipboardData.getData('Text').replace(/\D+/g, "");
                    page.fn.insertAtCaret(this, data);

                    e.preventDefault();
                    return false;
                })
                .on('keydown', function (e) {
                    -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 107, 110, 190]) || /65|67|86|88/.test(e.keyCode)
                    && (!0 === e.ctrlKey || !0 === e.metaKey)
                    || 35 <= e.keyCode && 40 >= e.keyCode/*Arrows*/
                    || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode)

                    && (96 > e.keyCode || 105 < e.keyCode)/*/NumPad*/
                    && (123 < e.keyCode || 112 > e.keyCode)/*F1-F12*/
                    && (e.shiftKey !== 0 && e.keyCode !== 61)/*plus*/
                    && (e.ctrlKey !== 0 && e.keyCode !== 90)/*ctrl +z*/
                    && (e.ctrlKey !== 0 && e.keyCode !== 45)/*ctrl insert*/
                    && (e.shiftKey !== 0 && e.keyCode !== 45)/*shift insert*/
                    //&&((e.ctrlKey||e.shiftKey)&&e.keyCode!==45)/* ctrl/shift + insert*/
                    && e.preventDefault();


                    var k = e.keyCode === 61 ? '+' : e.keyCode > 95 ? e.keyCode - 96 : e.keyCode - 48;
                    if (-1 !== $.inArray(k, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 61])) {
                        //console.log('disable keydown and insertAtCaret',e)
                        page.fn.insertAtCaret(this, k);
                        e.preventDefault();
                        return;
                    }
                });

            return;

        }
    },

    fn: {
        formatPhone: function (value, pattern) {
            pattern = typeof pattern !== 'undefined' ? pattern : '+7 (123) 456-78-90';
            var arr = value.match(/\d/g), i = 0;
            if (arr === null) return "";
            if (arr[0] === "8") arr[0] = "7";
            value = pattern.replace(/\d/g, function (a, b) {
                if (arr.length) i = b + 1;
                return arr.shift();
            }).substring(0, i);
            return value;
        },
        insertAtCaret: function (txtarea, text) {
            var scrollPos = txtarea.scrollTop;
            var strPos = 0;
            text = text.toString();
            var br = ((txtarea.selectionStart || txtarea.selectionStart === '0') ?
                "ff" : (document.selection ? "ie" : false));
            if (br === "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart('character', -txtarea.value.length);
                strPos = range.text.length;
            } else if (br === "ff") strPos = txtarea.selectionStart;

            var front = (txtarea.value).substring(0, strPos);
            var back = (txtarea.value).substring(strPos, txtarea.value.length);

            var newval = front + text + back;
            var formattedval = newval;

            txtarea.value = formattedval;
            strPos = strPos + text.length + (formattedval.length - newval.length);

            if (br === "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart('character', -txtarea.value.length);
                range.moveStart('character', strPos);
                range.moveEnd('character', 0);
                range.select();
            } else if (br === "ff") {
                txtarea.selectionStart = strPos;
                txtarea.selectionEnd = strPos;
                txtarea.focus();
            }
            txtarea.scrollTop = scrollPos;
        },
        insertPhoneAtCaret: function (txtarea, text) {
            if (!txtarea.value.length && text != 8 && text != 7 && text != '+')
                txtarea.value = '+7';
            var scrollPos = txtarea.scrollTop;
            var strPos = 0;
            text = text.toString();
            var br = ((txtarea.selectionStart || txtarea.selectionStart === '0') ?
                "ff" : (document.selection ? "ie" : false));
            if (br === "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart('character', -txtarea.value.length);
                strPos = range.text.length;
            } else if (br === "ff") strPos = txtarea.selectionStart;


            var front = (txtarea.value).substring(0, strPos);
            var back = (txtarea.value).substring(strPos, txtarea.value.length);

            var newval = front + text + back;
            var formattedval = page.fn.formatPhone(newval);

            txtarea.value = formattedval;
            strPos = strPos + text.length + (formattedval.length - newval.length);

            if (br === "ie") {
                txtarea.focus();
                var range = document.selection.createRange();
                range.moveStart('character', -txtarea.value.length);
                range.moveStart('character', strPos);
                range.moveEnd('character', 0);
                range.select();
            } else if (br === "ff") {
                txtarea.selectionStart = strPos;
                txtarea.selectionEnd = strPos;
                txtarea.focus();
            }
            txtarea.scrollTop = scrollPos;
        },
        declOfNum: function (number, titles) {
            var cases = [2, 0, 1, 1, 1, 2];
            return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
        },
        formatNum: function (a) {
            return a.toFixed(0).replace(/./g, function (c, i, a) {
                return i && c !== "." && !((a.length - i) % 3) ? ' ' + c : c;
            })
        },
        in_array: function (v, arr) {
            for (k in arr)
                if (typeof (v) == 'object') {
                    if (page.fn.in_array(arr[k], v))
                        return k;
                } else {
                    if (arr[k] == v)
                        return k;
                }

            return false;
        },

        first: function (obj) {
            for (var a in obj) {
                return a;
            }
        },
        numeric: function (e) {
            -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        },
        size: function (e) {

            if ($(this).val().length >= 1 * $(this).attr('size'))
                $(this).next('input').focus();
        }
    }
};