page.cart = {
    totalSum: 0,
    totalCnt: 0,
    init: function () {

        $('#createOrder').on('click', page.cart.submit);

        var cartCnt = $('.cartCnt');
        cartCnt.prev('i').on('click', page.cart.cntDec);
        cartCnt.next('i').on('click', page.cart.cntInc);
        cartCnt.on('keyup', page.cart.cnt);
        $('a.del', '#cartItems').on('click', page.cart.del);


        $('input.numeric').on('keydown', page.fn.numeric);
        $('input[size]').on('keyup', page.fn.size);

        page.cart.recalc();
    },

    cntDec: function () {
        var cartCnt = $(this).next('input.cartCnt');
        var v = $(cartCnt).val() * 1 - 1;
        if (v <= 0) v = 1;
        $(cartCnt).val(v);
        page.cart.recalc();
        return false;
    },
    cntInc: function () {
        var cartCnt = $(this).prev('input.cartCnt');
        $(cartCnt).val($(cartCnt).val() * 1 + 1);
        page.cart.recalc();
        return false;
    },
    recalc: function () {
        page.cart.totalSum = 0;
        page.cart.totalCnt = 0;

        $('tr.item', '#cartItems').each(function () {
            var input = $('input.cartCnt', this);
            var price = $(input).data('price');
            var sum = price * $(input).val();

            $('.price', this).text(page.fn.formatNum(price));
            $('.sum', this).text(page.fn.formatNum(sum));
            page.cart.totalSum += sum;
        });
        $('#cartSum').text(page.fn.formatNum(page.cart.totalSum));
    },
    del: function (event) {
        event.preventDefault();
        var tr = $(this).closest('tr').addClass('_removing');

        $.delete('/cart/', {'id': $(this).data('id')}, function () {

            if ($('tr', '#cartItems table tbody').length > 1) {
                $(tr).remove();
                page.cart.recalc();
            } else {
                window.location.reload();
            }
        }, "json");
    },

    submit: function (event) {
        event.preventDefault();

        $.post('/order/', $('input[name^="items"]', '#cartItems').serialize(), function (data) {
            if ('id' in data){
                window.location.href = '/order/' + data.id + '/';
            }
        }, "json");
    }
};