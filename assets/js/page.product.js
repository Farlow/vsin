page = page || {};
page.product = {
    init: function () {
        $('.add2cart', '.item').on('click', page.product.add2cart);

        $.get("/cart/", {}, page.product.cart.updateItems, "json");

    },
    add2cart(event) {
        event.preventDefault();

        if ($(this).hasClass('_adding')) {
            return;
        }
        var itemId = $(this).data('id');
        if (itemId in page.product.cart.items) {
            window.location = '/cart/';
            return;
        }
        $(this).addClass('_adding');
        page.product.cart.add(itemId);
    },
    cart: {
        items: {},
        add: function (itemId) {
            page.product.cart.sendData({"id": itemId}, page.product.cart.updateItems)
        },
        updateItems: function (data) {

            for (var n in data.cart.items) {
                var item = data.cart.items[n];
                page.product.cart.items[item[0]] = item;

                $('.add2cart', '#item'+data.cart.items[n][0])
                    .removeClass('_adding')
                    .addClass('inCart');
            }
        },
        sendData: function (postData, callback) {
            $.put("/cart/", postData, callback, "json");
        }
    }
};