page.order = {
    init: function () {
        $('#payOrder').on('click', page.order.pay);
    },
    pay: function (event) {
        event.preventDefault();
        $('#result').attr('class', '').text('Пробуем оплатить ...');
        var id = $(this).data('id');
        $.patch("/order/" + id + '/', {}, page.order.result, "json")
    },
    result: function (data) {
        if ('error' in data){
            $('#result').attr('class', 'error').text(data.error.message);
        }
        if ('status' in data && data.status === 'success'){
            $('#result').attr('class', 'success').text('Заказ успешно оплачен');
            $('#payOrder').remove();
        }
    }
};