<?php

require_once __DIR__."/vendor/autoload.php";

// cli-config.php
$isDevMode = true;

$conn = ['driver' => 'pdo_sqlite','path' => __DIR__ . '/db.sqlite'];
$config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(["./src/Entity/"], $isDevMode, null, null, false);



// obtaining the entity manager
$entityManager = \Doctrine\ORM\EntityManager::create($conn, $config);
$repo = $entityManager->getRepository(\App\Entity\Product::class);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($entityManager);

