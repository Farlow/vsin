<?php

namespace App;

use Doctrine\Common\Collections\ArrayCollection;
use ReflectionMethod;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader as DiYamlFileLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class Kernel
{
    /**
     * @var ContainerBuilder
     */
    private $containerBuilder;
    private $routeParameters;
    private $controllerName;
    private $controllerMethod;

    public function handle(Request $request): Response
    {
        try {
            $this->containerBuilder = new ContainerBuilder();
            $this->containerBuilder->set(Request::class, $request);
            $loader = new DiYamlFileLoader($this->containerBuilder, new FileLocator(__DIR__.'/../config/'));
            $loader->load('services.yaml');

            $routes = $this->loadRoutes();

            $context = (new RequestContext())->fromRequest($request);
            $matcher = new UrlMatcher($routes, $context);
            $this->routeParameters = $matcher->matchRequest($request);

            [$this->controllerName, $this->controllerMethod] = explode('::', $this->routeParameters['_controller']);

            $controller = $this->getController($this->controllerName);
            $controller->setContainer($this->containerBuilder);

            $arguments = $this->getMethodArguments($this->controllerName, $this->controllerMethod);
            $response = $controller->{$this->controllerMethod}(... $arguments);

        } catch (\Exception $exception) {
            $response = new Response(file_get_contents(__DIR__.'/../503.html'));
        }

        return $response;
    }

    private function loadRoutes(): RouteCollection
    {
        $fileLocator = new FileLocator([__DIR__.'/../config/']);
        $loader = new YamlFileLoader($fileLocator);

        return $loader->load('routes.yaml');
    }

    /**
     * @throws \ReflectionException
     */
    private function getController($controllerName): ControllerInterface
    {
        $className = "\\App\\Controller\\".$controllerName;
        $args = $this->getMethodArguments($controllerName);

        return new $className(...$args);
    }

    /**
     * @throws \ReflectionException
     */
    private function getMethodArguments($controllerName, $methodName = '__construct'): array
    {
        $className = "\\App\\Controller\\".$controllerName;

        $arguments = [];

        $reflection = new \ReflectionClass($className);
        $methods = new ArrayCollection($reflection->getMethods());
        $method = $methods->filter(
            function (\ReflectionMethod $method) use ($methodName) {
                return $method->getName() === $methodName;
            }
        )->first();

        /** @var ReflectionMethod $method */
        if ($method) {

            foreach ($method->getParameters() as $parameter) {
                /** @var \ReflectionParameter $parameter */
                $parameterType = $parameter->getType();
                $parameterName = $parameter->getName();

                if ($parameterType) {
                    $argumentClassName = $parameterType->getName();

                    if ($this->containerBuilder->has($argumentClassName)) {
                        $arguments[] = $this->containerBuilder->get($argumentClassName);
                    } elseif (class_exists($argumentClassName)) {
                        $arguments[] = new $argumentClassName;
                    }
                }

                if (!empty($this->routeParameters[$parameterName])) {
                    $arguments[] = $this->routeParameters[$parameterName];
                }
            }
        }

        return $arguments;
    }
}
