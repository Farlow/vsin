<?php

namespace App\Controller;

use App\AbstractController;
use App\ControllerInterface;
use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HomeController extends AbstractController implements ControllerInterface
{
    public function route(EntityManager $entityManager, Request $request): Response
    {
        $repository = $entityManager->getRepository(Product::class);

        return $this->render('pages/home.html.twig', [
            'products' => $repository->findAll()
        ]);
    }
}
