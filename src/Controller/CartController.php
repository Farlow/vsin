<?php

namespace App\Controller;

use App\AbstractController;
use App\ControllerInterface;
use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class CartController extends AbstractController implements ControllerInterface
{
    private $cartItemsIds;
    private $repository;

    public function __construct(Session $session, EntityManager $entityManager)
    {
        $this->cartItemsIds = $session->get('cart', []);
        $this->repository = $entityManager->getRepository(Product::class);

    }

    public function route(Request $request): Response
    {
        $cartItems = $this->repository->findByIds($this->cartItemsIds);

        return $this->render('pages/cart.html.twig', [
                'cart' => [
                    'items' => $cartItems,
                    'sum' => $cartItems->getSum(),
                ]
            ]);
    }

    public function getItems(): Response
    {
        $cartItems = $this->repository->findByIds($this->cartItemsIds);

        return new JsonResponse([
                'cart' => [
                    'sum' => $cartItems->getSum(),
                    'items' => $cartItems->toArray()
                ]
            ]);
    }

    public function addProduct(Session $session, Request $request): Response
    {
        $id = (int)$request->request->get('id');

        if (!empty($id)) {
            if (!in_array($id, $this->cartItemsIds)) {
                $this->cartItemsIds[] = $id;
                $session->set('cart', $this->cartItemsIds);
            }
        }

        return $this->getItems();
    }

    public function removeProduct(Session $session, Request $request): Response
    {
        $id = (int)$request->request->get('id');

        if (!empty($id)) {

            if (false !== $key = array_search($id, $this->cartItemsIds)) {
                unset($this->cartItemsIds[$key]);
                $session->set('cart', $this->cartItemsIds);
            }
        }

        return $this->getItems();
    }
}