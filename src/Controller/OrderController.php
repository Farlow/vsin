<?php

namespace App\Controller;

use App\AbstractController;
use App\ControllerInterface;
use App\Entity\Order;
use App\Entity\OrderItem;
use App\Entity\Product;
use App\Exception\OrderNotFoundException;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

class OrderController extends AbstractController implements ControllerInterface
{
    private $orders;
    private $entityManager;

    public function __construct(Session $session, EntityManager $entityManager)
    {
        $this->orders = $session->get('orders', []);
        $this->entityManager = $entityManager;
    }

    public function create(Request $request, Session $session): Response
    {
        $orderItems = $request->request->get('items');

        $order = $this->createOrder($orderItems);

        $this->orders[] = $order->getId();

        $session->set('orders', $this->orders);
        $session->set('cart', []);

        return new JsonResponse(
            [
                'id' => $order->getId(),
            ]
        );
    }

    private function createOrder(array $orderItems): Order
    {
        $productRepository = $this->entityManager->getRepository(Product::class);
        $order = new Order();

        foreach ($orderItems as $productId => $count) {
            $product = $productRepository->find($productId);
            $orderItem = new OrderItem();
            $orderItem
                ->setOrder($order)
                ->setProduct($product)
                ->setPrice($product->getPrice())
                ->setCount($count);
            $order->addItem($orderItem);
        }

        $this->entityManager->persist($order);
        $this->entityManager->flush();

        return $order;
    }

    /**
     * @throws OrderNotFoundException
     */
    public function view(int $id): Response
    {
        if (!in_array($id, $this->orders)) {
            throw new OrderNotFoundException();
        }

        $order = $this->entityManager->getRepository(Order::class)->find($id);

        if (!$order) {
            throw new OrderNotFoundException();
        }

        return $this->render(
            'pages/order.html.twig',
            [
                'order' => $order
            ]
        );
    }

    public function pay(int $id): Response
    {
        if (!in_array($id, $this->orders)) {
            $this->orderNotFound();
        }

        $order = $this->entityManager->getRepository(Order::class)->find($id);

        if (!$order) {
            $this->orderNotFound();
        }

        return $this->payOrder($order);
    }

    private function orderNotFound(): JsonResponse
    {
        return new JsonResponse(
            [
                'error' => [
                    'code' => 404,
                    'message' => 'Order not found',
                ],
            ]
        );
    }

    private function payOrder(Order $order): JsonResponse
    {
        $url = random_int(0, 9) > 5 ? 'https://ya.ru' : 'https://ya.ruu';

        try {
            $client = new Client();
            $resource = $client->request('GET', $url);
            if ($resource->getStatusCode() !== 200) {
                throw new \Exception();
            }

            $order->setStatus('pay');
            $this->entityManager->persist($order);
            $this->entityManager->flush();

            return new JsonResponse(['status' => 'success']);

        } catch (\Exception $exception) {
            return new JsonResponse(
                [
                    'error' => [
                        'code' => 503,
                        'message' => 'Try to repeat later',
                    ],
                ]
            );
        }
    }
}
