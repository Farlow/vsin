<?php

namespace App\Collection;

use App\Entity\Product;
use Doctrine\Common\Collections\ArrayCollection;

class CartCollection extends ArrayCollection
{
    public function getSum(): int
    {
        $sum = 0;
        $this->map(function (Product $product) use (&$sum) {
            $sum += $product->getPrice();
        });

        return $sum;
    }
}
