<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 * @ORM\Table(name="products")
 */
class Product implements \JsonSerializable
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @ORM\Column(name="image", type="string", length=255, nullable=false)
     */
    private $image;

    /**
     * @ORM\Column(name="price", type="integer", nullable=false)
     */
    private $price;

    /**
     * @ORM\Column(name="active", type="integer", nullable=true)
     */
    private $active;

    /**
     * @ORM\Column(name="available", type="integer", nullable=true)
     */
    private $available;

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getActive(): int
    {
        return $this->active;
    }

    public function getAvailable(): int
    {
        return $this->available;
    }

    public function jsonSerialize()
    {
        return [
            $this->getId(),
            $this->getName(),
            $this->getPrice(),
        ];
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}
