<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * Product
 *
 * @ORM\Entity()
 * @ORM\Table(name="orders")
 */
class Order
{
    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(name="status", type="text", length=255, nullable=false)
     */
    private $status;

    /**
     * @ORM\OneToMany(targetEntity="\App\Entity\OrderItem", mappedBy="order", cascade={"persist"})
     */
    private $items;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->items = new ArrayCollection();
        $this->status = 'new';
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatusTitle(): ?string
    {
        return [
            'new' => 'Новый',
            'pay' => 'Оплачен',
        ][$this->status];
    }

    public function getItems(): PersistentCollection
    {
        return $this->items;
    }

    public function addItem(OrderItem $orderItem)
    {
        $this->items->add($orderItem);

        return $this;
    }

    public function getSum(): int
    {
        $sum = 0;
        $this->items->map(
            function (OrderItem $orderItem) use (&$sum) {
                $sum += $orderItem->getSum();
            }
        );

        return $sum;
    }
}