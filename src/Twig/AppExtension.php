<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter('price', [$this, 'price'], ['is_safe' => ['html']]),
            new TwigFilter('date', [$this, 'date'], ['is_safe' => ['html']]),
        ];
    }

    public function price(?int $price = null): string
    {
        if (empty($price))
            return "";

        return number_format($price, 0, ".", " ");
    }

    /**
     * @throws \Exception
     */
    public function date($dateTime): string
    {
        if (is_int($dateTime) || is_string($dateTime)) {
            $dateTime = new \DateTime('@'.$dateTime);
        }
        return \IntlDateFormatter::formatObject($dateTime, 'dd MMM Y г.', 'ru_RU');
    }
}
