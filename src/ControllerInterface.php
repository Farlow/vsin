<?php

namespace App;

use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

interface ControllerInterface
{
    public function render($view, $parameters, Response $response): Response;

    public function setContainer(ContainerInterface $container): void;
}
