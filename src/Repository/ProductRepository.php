<?php

namespace App\Repository;

use App\Collection\CartCollection;
use Doctrine\DBAL\Connection as DBAL;
use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findAll(): array
    {
        return parent::findBy(
            [
                'active' => 1,
                'available' => 1,
            ]
        );
    }

    public function findByIds(array $cartItemsIds): CartCollection
    {
        $qb = $this
            ->getEntityManager()
            ->createQueryBuilder();

        $qb = $qb
            ->select('product')
            ->from(\App\Entity\Product::class, 'product')
            ->andWhere($qb->expr()->isNotNull('product.active'))
            ->andWhere($qb->expr()->isNotNull('product.available'))
            ->andWhere($qb->expr()->in('product.id', ':ids'))
            ->setParameter(':ids', $cartItemsIds, DBAL::PARAM_INT_ARRAY);

        return new CartCollection($qb->getQuery()->getResult());
    }
}
